package main

import (
	"fmt"
	"sync"
	"time"
)

const conferenceTickets int = 40

var conferenceName = "Go Conference"
var remainingTickets uint = 40
var bookings = make([]UserData, 0)

type UserData struct {
	firstName       string
	lastName        string
	email           string
	numberOfTickets uint
}

var wg = sync.WaitGroup{}

func main() {
	// fmt.Printf("conferenceTickets is %T, remainingTickets is %T, conferenceName is%T\n",
	// 	conferenceTickets, remainingTickets, conferenceName)

	greetUsers()

	firstName, lastName, email, userTickets := getUserInput()
	isValidName, isValidEmail, isValidTicketNumber := validateUserInput(firstName, lastName, email, userTickets, remainingTickets)
	// fmt.Println(conferenceName)
	// for {
	if isValidName && isValidEmail && isValidTicketNumber {

		bookTicket(userTickets, firstName, lastName, email)

		wg.Add(1)
		go sendTicket(userTickets, firstName, lastName, email)

		// if userTickets <= remainingTickets {
		// remainingTickets = remainingTickets - userTickets
		// bookings[0] = firstName + " " + lastName
		// bookings = append(bookings, firstName+" "+lastName)

		// fmt.Printf("the whole slice: %v\n", bookings)
		// fmt.Printf("the first value: %v\n", bookings[0])
		// fmt.Printf("slice type: %v\n", bookings[0])
		// fmt.Printf("slice length: %v\n", len(bookings))

		// fmt.Printf("thankyou %v %v for booking tickets. we'll send the reciept to %v\n", firstName, lastName, email)
		// fmt.Printf("%v tickets remaining\n", remainingTickets)

		firstNames := getFirstNames()
		fmt.Printf("The first names of bookings are %v\n", firstNames)

		// firstNames := []string{}
		// for _, booking := range bookings {
		// 	var names = strings.Fields(booking)
		// 	firstNames = append(firstNames, names[0])
		// }

		if remainingTickets == 0 {
			fmt.Println("Our conference is booked out.")
			// break
		}
	} else {
		if !isValidName {
			fmt.Println("first name or last name you entered is too short")
		}
		if !isValidEmail {
			fmt.Println("email address you entered doesn't contain @ sign")
		}
		if !isValidTicketNumber {
			fmt.Println("number of tickets you entered is invalid")
		}
	}
	wg.Wait()

	// fmt.Printf("Welcome to %v ! \n", conferenceName)
	// fmt.Printf("%v is the best of Online shopping apps conference\n", conferenceName)
	// fmt.Printf("we have total of %v tickets and %v are still available\n", conferenceTickets, remainingTickets)
	// fmt.Println("Get your ticket here to attend")

	// var firstName string
	// var lastName string
	// var email string
	// var userTickets uint

	// fmt.Println("Enter your first name: ")
	// fmt.Scan(&firstName)

	// fmt.Println("Enter your last name: ")
	// fmt.Scan(&lastName)

	// fmt.Println("Enter your email address: ")
	// fmt.Scan(&email)

	// fmt.Println("Enter number of tickets: ")
	// fmt.Scan(&userTickets)

	// if userTickets > remaining {
	// 	fmt.Printf("we only have %v tickets remaining,
	// 	so you cant book %v tickets \n", remainingTickets, userTickets)
	// 	continue
	// }
	// firstName, laslastName, email, userTickets ;= getUserInput()
	// isValidName := len(firstName) >= 2 && len(lastName) >= 2
	// isValidEmail := strings.Contains(email, "@")
	// isValidTicketNumber := userTickets > 0 && userTickets <= remainingTickets

	// city := "London"

	// switch city {
	// case "New York":
	// 	// booking for New York conference
	// case "Singapore", "Hong Kong":
	// 	// booking for Singapore & Hong Kong conference
	// case "London", "Berlin":
	// 	// booking for London & Berlin conference
	// case "Mexico City":
	// 	// booking for Mexico City conference
	// default:
	// 	fmt.Print("No valid city selected")
	// }

	// fmt.Printf("tthe first names of bookings: %v\n", firstName)
	// if userTickets > remainingTickets {

	// return firstName, lastName, email, userTickets

	// userName = "Elyakim"
	// userTickets = 4
	// fmt.Printf("User %v booked %v tickets.\n", userName, userTickets)
}

func greetUsers() {
	fmt.Printf("Welcome to %v ! \n", conferenceName)
	fmt.Printf("%v is the best of Online shopping apps conference\n", conferenceName)
	fmt.Printf("we have total of %v tickets and %v are still available\n", conferenceTickets, remainingTickets)
	fmt.Println("Get your ticket here to attend")
}

func getFirstNames() []string {
	firstNames := []string{}

	for _, booking := range bookings {
		// var names = strings.Fields(booking)
		firstNames = append(firstNames, booking.firstName)
	}
	return firstNames
}

// func validateUserInput(firstName string, lastName string, email string, userTickets uint, remainingTickets uint) (bool, bool, bool) {
// 	isValidName := len(firstName) >= 2 && len(lastName) >= 2
// 	isValidEmail := strings.Contains(email, "@")
// 	isValidTicketNumber := userTickets > 0 && userTickets <= remainingTickets

// 	return isValidName, isValidEmail, isValidTicketNumber
// }

func getUserInput() (string, string, string, uint) {
	var firstName string
	var lastName string
	var email string
	var userTickets uint

	fmt.Println("Enter Your First Name: ")
	fmt.Scanln(&firstName)

	fmt.Println("Enter Your Last Name: ")
	fmt.Scanln(&lastName)

	fmt.Println("Enter Your Email: ")
	fmt.Scanln(&email)

	fmt.Println("Enter number of tickets: ")
	fmt.Scanln(&userTickets)

	return firstName, lastName, email, userTickets
}

func bookTicket(userTickets uint, firstName string, lastName string, email string) {
	remainingTickets = remainingTickets - userTickets

	var userData = UserData{
		firstName:       firstName,
		lastName:        lastName,
		email:           email,
		numberOfTickets: userTickets,
	}

	bookings = append(bookings, userData)
	fmt.Printf("List of bookings is %v\n", bookings)
	fmt.Printf("Thank you %v for booking %v tickets. we'll send the confirmation to email %v\n", firstName, userTickets, email)
	fmt.Printf("%v tickets remaining for\n", remainingTickets)
}

func sendTicket(userTickets uint, firstName string, lastName string, email string) {
	time.Sleep(10 * time.Second)
	var ticket = fmt.Sprintf("%v tickets for %v %v", userTickets, firstName, lastName)
	fmt.Println("#################")
	fmt.Printf("Sending ticket:\n %v \nto email address %v\n", ticket, email)
	fmt.Println("#################")
	wg.Done()
}
